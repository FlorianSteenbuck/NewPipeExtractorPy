'''
MIT License

Copyright (c) 2018 Florian Steenbuck

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''
from newpipe import get_downloader

class Extractor:
    __service = None
    __originalUrl = None

    __cleanUrl = None
    __pageFetched = False
    __downloader = None

    def __init__(self, service, url):
        if service == None:
            raise Exception("service is None")
        if url == None:
            raise Exception("url is None")
        self.__service = service
        self.__originalUrl = url
        self.__downloader = get_downloader()
        if self.__downloader == None:
            raise Exception("downloader is None")

    def get_url_id_handler(self):
        pass

    def fetch_page(self):
        if self.__pageFetched:
            return
        self.on_fetch_page()
        self.__pageFetched = True

    def _assert_page_fetched(self):
        if not self.__pageFetched:
            raise Exception("Page is not fetched. Make sure you call fetch_page()")

    def _is_page_fetched(self):
        return self.__pageFetched

    def on_fetch_page(self):
        pass

    def get_id(self):
        pass

    def get_name(self):
        pass

    def get_clean_url(self):
        if self.__cleanUrl is not None and len(self.__cleanUrl) == 0:
            return self.__cleanUrl

        try:
            self.__cleanUrl = self.get_url_id_handler().clean_url(self.__originalUrl)
        except Exception:
            self.__cleanUrl = None
            # fallback to the original url
            return self.__originalUrl

        return self.__cleanUrl

    def get_service(self):
        return self.__service

    def get_service_id(self):
        return self.__service.get_service_id()