'''
MIT License

Copyright (c) 2018 Florian Steenbuck

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''
class Collector:
    _I = None
    _E = None

    '''
        Collectors are used to simplify the collection of information
        from extractors
        @param <I> the item type
        @param <E> the extractor type
    '''

    def __init__(self, I, E):
        if not hasattr(I, '__name__'):
            raise Exception("I is not a class")
        if not hasattr(E, '__name__'):
            raise Exception("E is not a class")
        self._E = E
        self._I = I

    def _raiseOnNotE(self, e):
        if not isinstance(e, self._E):
            raise Exception("extractor is not instanceof E \"" + self._E + "\"")
        return e

    def _raiseOnNotI(self, i):
        if not isinstance(i, self._I):
            raise Exception("item is not instanceof I \"" + self._I + "\"")
        return i

    def _raiseOnNotExceptionList(self, elist):
        if type(elist) != list:
            raise Exception("exception list is not a list")

        for e in elist:
            if not isinstance(e, Exception):
                raise Exception("some exception of exception list is not instanceof Exception")

        return elist

    def _raiseOnNotIList(self, ilist):
        if type(ilist) != list:
            raise Exception("item list is not a list")

        for i in ilist:
            if not isinstance(i, self._I):
                raise Exception("some item of item list is not instanceof I \"" + self._I + "\"")

        return ilist

    def getI(self):
        return self._I

    def getE(self):
        return self._E

    '''
        Try to add an extractor to the collection
        @param extractor the extractor to add
    '''
    def commit(self, extractor):
        self._raiseOnNotE(extractor)
        self._commit(extractor)

    def _commit(self, extractor):
        pass

    '''
        Try to extract the item from an extractor without adding it to the collection
        @param extractor the extractor to use
        @return the item
        @throws ParsingException thrown if there is an error extracting the
                                 <b>required</b> fields of the item. 
    '''
    def extract(self, extractor):
        self._raiseOnNotE(extractor)
        return self._raiseOnNotI(self._extract(extractor))

    def _extract(self, extractor):
        pass

    '''
        Get all items
        @return the items
    '''
    def get_items(self):
        return self._raiseOnNotIList(self._get_items())

    def _get_items(self):
        pass

    '''
        Get all errors
        @return the errors
    '''
    def get_errors(self):
        return self._raiseOnNotExceptionList(self._get_errors())

    def _get_errors(self):
        pass

    # Reset all collected items and errors
    def reset(self):
        pass