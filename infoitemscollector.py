'''
MIT License

Copyright (c) 2018 Florian Steenbuck

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''
from collector import Collector
from infoitem import InfoItem
from exceptions.foundad import FoundAdException
from exceptions.parsing import ParsingException

class InfoItemsCollector(Collector):
    __itemList = list()
    __errors = list()
    __serviceId = None

    def __init__(self, I, E, serviceId):
        Collector.__init__(self, I, E)
        if not issubclass(I, InfoItem):
            raise Exception("I is not subclass of InfoItem")
        self.__serviceId = serviceId

    def _get_items(self):
        return self.__itemList

    def _get_errors(self):
        return self.__errors

    def reset(self):
        self.__itemList.clear()
        self.__errors.clear()

    def add_error(self, error):
        self.__errors.append(error)

    def add_item(self, item):
        self._raiseOnNotI(item)
        self.__itemList.append(item)

    def get_service_id(self):
        return self.__serviceId

    def _commit(self, extractor):
        try:
            self.add_item(self.extract(extractor))
        except FoundAdException as e:
            # found an ad. Maybe a debug line could be placed here
            pass
        except ParsingException as ex:
            self.add_error(ex)