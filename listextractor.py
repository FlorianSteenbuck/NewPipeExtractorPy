'''
MIT License

Copyright (c) 2018 Florian Steenbuck

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''
from extractor import Extractor
from infoitem import InfoItem

class ListExtractor(Extractor):
    __R = None

    def __init__(self, R):
        if not hasattr(R, '__name__'):
            raise Exception("R is not a class")
        self.__R = R

    def get_initial_page(self):
        return self._get_initial_page()

    def _get_initial_page(self):
        pass

    def get_next_page_url(self):
        pass

    def get_page(self, nextPageUrl):
        return self._get_page(nextPageUrl)

    def _get_page(self, nextPageUrl):
        pass

    def has_next_page(self):
        nextPageUrl = self.get_next_page_url()
        return nextPageUrl is not None and len(nextPageUrl) != 0

    class InfoItemsPage:
        __EMPTY = list()
        __itemsList = None
        __nextPageUrl = None
        __errors = None

        def __init__(self, T, itemsList, nextPageUrl, errors):
            self._raiseOnT(T)
            if not type(itemsList) == list:
                raise Exception("itemsList is not a list")
            for item in itemsList:
                if not isinstance(item, T):
                    raise Exception("some item of itemsList is not instanceof T")
            self.__itemsList = itemsList
            self.__nextPageUrl = nextPageUrl
            self.__errors = errors

        def __init__(self, T, collector, nextPageUrl):
            self._raiseOnT(T)
            if issubclass(collector.getI(), T):
                raise Exception("collector I is not subclass of T")
            self.__init__(T, collector.get_items(), nextPageUrl, collector.get_errors())

        def _raiseOnT(self, T):
            if not hasattr(T, '__name__'):
                raise Exception("T is not a class")
            if not issubclass(T, InfoItem):
                raise Exception("T is not subclass of InfoItem")

        def empty_page(self):
            return self.__EMPTY

        def has_next_page(self):
            return self.__nextPageUrl is not None and len(self.__nextPageUrl) != 0

        def get_next_page_url(self):
            return self.__nextPageUrl

        def get_errors(self):
            return self.__errors