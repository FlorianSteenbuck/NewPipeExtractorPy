'''
MIT License

Copyright (c) 2018 Florian Steenbuck

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''
class Info:
    __serviceId = None

    __id = None
    __url = None
    __name = None

    __errors = list()

    def add_error(self, raiseable):
        self.__errors.append(raiseable)

    def add_all_errors(self, raiseables):
        for raiseable in raiseables:
            self.add_error(raiseable)

    def __init__(self, serviceId, id, url, name):
        self.__serviceId = serviceId
        self.__id = id
        self.__url = url
        self.__name = name

    def __str__(self):
        return self.__class__.__name__ + "[url=\"" + self.__url + "\", name=\"" + self.__name + "\"]"

    def get_service_id(self):
        return self.__serviceId

    def get_id(self):
        return self.__id

    def get_url(self):
        return self.__url

    def get_name(self):
        return self.__name

    def get_errors(self):
        return self.__errors